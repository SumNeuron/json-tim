import {
  identity, timsort, sortableRecords, isRecordSortable,
  stringSort, numberSort, simpleSort
} from './jsontim/timsort.js'

let jsontim = {
  identity, timsort, sortableRecords, isRecordSortable,
  stringSort, numberSort, simpleSort
}

export default jsontim
