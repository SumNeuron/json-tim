import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify-es';
import minimist from 'minimist';
import pkg from './package.json';

const argv = minimist(process.argv.slice(2));

const config = {
  input: 'src/index.js',
  extend: true,
  output: {
    name: pkg.name,
    exports: 'named',
  },
  plugins: [
    babel({
      exclude: 'node_modules/**',
      externalHelpers: true,
      plugins: [
        [
          'wildcard',
          {
            exts: [],
            nostrip: true,
          },
        ],
        '@babel/plugin-external-helpers',
      ],
      presets: [
        [
          '@babel/preset-env',
          {
            modules: false,
          },
        ],
      ],
    }),
  ],
};

// Only minify browser (iife) version
if (argv.format === 'iife') {
  config.plugins.push(uglify());
}

export default config;
